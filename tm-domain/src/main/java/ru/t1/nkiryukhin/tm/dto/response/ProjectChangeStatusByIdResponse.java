package ru.t1.nkiryukhin.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectChangeStatusByIdResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIdResponse(@Nullable ProjectDTO project) {
        super(project);
    }

}