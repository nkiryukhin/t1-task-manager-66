package ru.t1.nkiryukhin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface TaskRestEndpoint {

    @Nullable
    Collection<Task> findAll();

    void save(@NotNull Task task);

    @Nullable
    Task findById(@NotNull String id);

    boolean existsById(@NotNull String id);

    long count();

    void deleteById(@NotNull String id);

    void delete(@NotNull Task task);

    void clear(List<Task> tasks);

    void clear();

}
