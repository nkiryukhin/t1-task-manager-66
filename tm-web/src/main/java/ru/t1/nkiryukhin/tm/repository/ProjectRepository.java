package ru.t1.nkiryukhin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.model.Project;


@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {
}