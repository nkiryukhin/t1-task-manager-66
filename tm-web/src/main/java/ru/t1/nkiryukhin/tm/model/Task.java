package ru.t1.nkiryukhin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.nkiryukhin.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public final class Task {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    @NotNull
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    public Task(@NotNull final String name) {
        this.name = name;
    }

    public Task(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    public Task(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status
    ) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @Override
    public String toString() {
        return name + " : " + description + " : " + status.getDisplayName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return created.equals(task.created)
                && name.equals(task.name)
                && Objects.equals(project, task.project)
                && status == task.status;
    }

}