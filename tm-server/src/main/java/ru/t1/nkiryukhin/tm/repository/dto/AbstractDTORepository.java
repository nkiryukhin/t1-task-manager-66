package ru.t1.nkiryukhin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.dto.model.AbstractModelDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface AbstractDTORepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

    long count();

    void delete(@NotNull final M model);

    void deleteAll();

    void deleteById(@NotNull final String id);

    boolean existsById(@NotNull final String id);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull final Sort sort);

    @NotNull
    Optional<M> findById(@NotNull final String id);

}
