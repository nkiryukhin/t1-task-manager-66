package ru.t1.nkiryukhin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.model.User;

import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    Optional<User> findByLogin(@NotNull final String login);

    @Nullable
    Optional<User> findByEmail(@NotNull final String email);

}
