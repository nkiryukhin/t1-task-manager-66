package ru.t1.nkiryukhin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;


@Repository
@Scope("prototype")
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull final String login);

    @Nullable
    UserDTO findByEmail(@NotNull final String email);

}
