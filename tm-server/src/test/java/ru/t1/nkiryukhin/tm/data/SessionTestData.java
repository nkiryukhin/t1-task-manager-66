package ru.t1.nkiryukhin.tm.data;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.model.Session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static Session USUAL_SESSION1 = new Session();

    @NotNull
    public final static Session USUAL_SESSION2 = new Session();

    @NotNull
    public final static Session ADMIN_SESSION1 = new Session();

    @NotNull
    public final static Session ADMIN_SESSION2 = new Session();

    @Nullable
    public final static Session NULL_SESSION = null;

    @NotNull
    public final static String NON_EXISTING_SESSION_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<Session> USER_SESSION_LIST = Arrays.asList(USUAL_SESSION1, USUAL_SESSION2);

    @NotNull
    public final static List<Session> ADMIN_SESSION_LIST = Arrays.asList(ADMIN_SESSION1, ADMIN_SESSION2);

    @NotNull
    public final static List<Session> SESSION_LIST = new ArrayList<>();

    static {
        USER_SESSION_LIST.forEach(session -> session.setUser(UserTestData.USUAL_USER));
        USER_SESSION_LIST.forEach(session -> session.setRole(Role.USUAL));
        ADMIN_SESSION_LIST.forEach(session -> session.setUser(UserTestData.ADMIN_USER));
        ADMIN_SESSION_LIST.forEach(session -> session.setRole(Role.ADMIN));
        SESSION_LIST.addAll(USER_SESSION_LIST);
        SESSION_LIST.addAll(ADMIN_SESSION_LIST);
    }

}